VenenuX Teaiso
==============

**The ISO creation tool for VenenuX development image creation**

## Notes

#### VenenuX-teaiso vs teaiso

* We only provide and works with two kind of profiles.. alpine and debian check [Manual-of-usage.md#profiles](Manual-of-usage.md#profiles)
* Set a default user "general" and password "general" always , no matter the customized script
* Set only alpine profiles, all the customizations are made for alpine and debian unstable
* Set default skeleton and packages for base console settings, to check if those changes in future

#### Social networks

VenenuX has many social network ALL OF THEM ALMOST LINKED with [![chatroom icon](https://patrolavia.github.io/telegram-badge/chat.png)](https://t.me/venenux), 
but development for spanish are more active in [![chatroom icon](https://patrolavia.github.io/telegram-badge/chat.png)](https://t.me/latam_programadores). 

The venenux Twitter account are only used to promoted mayor anunces and articles [![Twitter VenenuX](https://img.shields.io/twitter/follow/vegnuli}?style=flat&logo=twitter)](https://twitter.com/vegnuli)

For complete social network and contact please visit the [![Codeberg VenenuX](https://img.shields.io/badge/Codeberg-2185D0?style=flat&logo=Codeberg&logoColor=white)](https://codeberg.org/venenux/venenux-docus)

Original project has a active telegram group: [![chatroom icon](https://patrolavia.github.io/telegram-badge/chat.png)](https://t.me/iso_calismalari) 
where main language is Turkish but allowed English too. The Tearch linux 
is a related group comunity [![channel icon](https://patrolavia.github.io/telegram-badge/follow.png)](https://t.me/TeArchlinux)

#### Who are using teaiso?

* **Tearch Linux ISOS**: This project is widely used to produce the [TeArchlinux Releases](https://github.com/tearch-linux/releases) and is part of the [TeArchlinux Project](https://tearch-linux.github.io/)
* **VenenuX Alpine ISOS**: This project is widely used to produce the VenenuX Alpine ISOs https://codeberg.org/alpine/alpine-isomaker using their own forked version https://gitlab.com/venenux/venenux-teaiso
* **PUFF OS ISOS**: This project is widely used to produce the [PUFFOS releases](https://github.com/PuffOS/teaiso-profile/releases/) and have they own forked profile definition.

# FAQ

#### How can i contribute

Please check [Contributing.md](Contributing.md) document.

#### Installer of debian or alpine does not work

Teaiso created images are Live ISO focused and does not handle 
instalation procedures. It only manages the live process boot 
and during the boot only manages the mounts to fit the live environment.

Because of that, all the references of the installers are online only, 
so if you try to use the installers without a network internet, 
those will fail.

#### How can i install my Teaiso images?

Teaiso is just a Live ISO image creator, each installer of each Linux flavor 
are out of scope of the project, you have three options:

- Try the natural installer, in alpine just works, in debian does not yet tested to work
- Use the [live-build package](https://packages.debian.org/search?keywords=live-build) but the provides by VenenuX
- Use the [17g installer](http://gitlab.com/ggggggggggggggggg/17g) a fork of LMDE installer

In some special cases, due the installer nature are included in the rootfs, installation 
can be posible (by example Alpine script `setup-alpine` will work, check next FAQ item)

#### My ISO do not have networking!

Some cases are special, by example alpine init system is complete overridden, so the normal 
init process is not the same, that's the reason `teaiso` created images of alpine linux 

The networking init script will conflict with `networkmanager` management, so 
only has networking if you included `networkmanager` package in the live iso creation 
as the most usefully solution, if not, you must setup the networkin package, 
and get sure that have all the necesary packages for alpine or debian like `wpa_supplicant`

Unfortunatelly only network-manager offers good behaviour, so is mandatory to 
always include in your ISO creation that package and we recommend that you 
have wired connection or simple WPA password wifi, with well supported free modules.

#### Usage of DESTDIR at install

Please understand that although you can set `$DESTDIR` at installation procedure, 
the developer must set paths to the root, and this variable is only used to manage 
the destination of files when performed the install, not the execution of the program.

#### Grub notes

Teaiso uses grub for the creation of the ISO images, so it will install or will need the full Grub suite. 
For example, this means that if you install Teaiso and you don't have grub-firmware on Debian, 
it will be installed or will be required. This is only for the creation of the ISO boot not for the Live Disk roofs.

The isos internally must have included the grub boot loader, cos as 
we previously pointed, may not have networking untill you install 
and setup property the network, so the grub boot loader necesary files 
must be present to property install the iso, at least for alpine flavours.

## See also:

* [Teaiso-technology.md](Teaiso-technology.md)
