VenenuX Teaiso
==============

**The ISO creation tool for VenenuX development image creation**

## Contributing

If you're interested in contributing to this project, first of all you must be 
a root linux user, and also do not use any other OS rather than Linux Debian based 
or at least Alpine linux based. Please while many others Debian derivates are valid, 
be aware that even if they are supported they are not officially accepted, 
the program will work on them but they are not valid.

#### How to contribute

Before contributing to this project, please take a bit of time to read our . 
Also, note that this project is a fork of the original one, check [README.md - about the project](README.md#about-the-project)

Contribution guidelines are the same as main VenenuX project, mostly anti windos things.

##### Repositories of source code

* https://codeberg.org/venenux/venenux-teaiso
* https://gitlab.com/venenux/venenux-teaiso

##### Workflow basics

1. Join at Gitlab or codeberg (using the links above)
2. Fork the project (using the links above) by the fork button
3. Create a branch in your repository.
4. Commit and push your changes to your branch.
5. Create a Pull Request (codeberg) or a Merge Request (gitlab) from your new brand
6. Wait for the Merge your pull request, and later delete your branch or the fork

##### Sync your work

If you are working in large changes, then before step (4) and during of, 
you must made **sync** of your brand respect original project, this is 
just using `pull` fast foward from original by the `git remote` management command.
Also if your changes are too old you can **sync** by the `rebasing`.

#### Project Structure

The project is mainly composed by one , hosted on Gitlab/codeberg. 

The technology are well described in [Teaiso-technology.md](Teaiso-technology.md) document.

## See also

* [Teaiso-technology.md](Teaiso-technology.md)
