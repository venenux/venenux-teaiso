VenenuX Teaiso
==============

**The ISO creation tool for VenenuX development image creation**

## Installation

Currently **Teaiso** is packaged for VenenuX in [VenenuX OSB repositories](https://build.opensuse.org/project/show/home:vegnuli), 
but is distribution agnostic.. so will work in any Linux distribution, 
including those with `muslc` or `glibc` library.

The process does not need much RAM neither DISK space.. but when performed runtime 
will need so much as you wants into each ISO generation.

## Requirements

#### Build

* git
* wget
* gcc
* make

#### Runtime

* bash
* chroot
* coreutils
* python3
* busybox
* mtools
* xorriso
* grub ([check notes at FAQ](FAQ-and-notes.md#grub-notes))
* squashfs-tools

## Installation

If you have enabled [VenenuX OSB repositories](https://build.opensuse.org/project/show/home:vegnuli), 
just run `apt update && apt install venenux-teaiso`

The installation only places files in two places, for more information review [Teaiso technology paths](Teaiso-technology.md#paths)

Please read about the [usage of `$DESTDIR` variable at FAQ-and-notes.md](FAQ-and-notes.md#usage-of-destdir-at-install).

```
git clone https://codeberg.org/venenux/venenux-teaiso

cd venenux-teaiso

make install
```

## See also:

* [Manual-of-usage.md](Manual-of-usage.md)
