VenenuX Teaiso
==============

**The ISO creation tool for VenenuX development image creation**

## About the manual

The main program is `makeiso`, the program will produce an ISO boot image file and must be parse a default profile (a template Linux flavor) to produce. 

The complete terminology and step by step documentation are into [Teaiso-technology.md](Teaiso-technology.md).

## Help of the program

```
Usage: makeiso -p=PROFILE [OPTION]...
ISO generation tool for GNU/Linux.
Example: makeiso -p=alpine --interactive
Profile directory should contain profile.yaml.

Base Arguments:
  -p=PROFILE, --profile=PROFILE     Profile directory or name (default: archlinux)
  -o=OUTPUT, --output=OUTPUT        ISO output directory (default: /var/lib/makeiso/output)
  -w=WORK, --work=WORK              ISO work directory (default: /var/lib/makeiso/work)
  -c=BASE, --create=BASE            Create profile by base profile
  -g=KEY, --gpg=KEY                 Sign airootfs image by GPG

Miscellaneous:
  -h, --help                        Display this help text and exit
      --version                     Display version and exit
      --nocolor                     Disable colorized output
      --simulate                    Enable simulation mode
      --nocheck                     Skip all check
      --interactive                 Interactive operations
      --debug                       Enable debug mode
```

## Usage

Its quite simple.. cos just generate a ISO image from debian/alpine testing with a default user:

`makeiso`

This will output in `/var/lib/makeiso` a ISO file.

The format of the ISO file will be `<profile-name>-<date as YYYY-MM-dd>.iso`

Profiles are the type of linux flavors that are created, which defaults to alpine if only the command is invoked.

#### Profiles
 
The profiles are **directories that provides the nature of ISO that will be created**, the format is well described in the document [creating-profile.rst](creating-profile.rst).

These are the available **templates for each supported ["distro" (Linux distribution)](Teaiso-technology.md#terminology) base [profile](Teaiso-technology.md#profiles-definitions)**:

| Template name | Profile distro  | observations                 |
| ------------ | ---------------- | ---------------------------- |
| alpine       | Alpine Linux     | power of x86_64 minimal 280MB console image 😳 |
| debian       | Debian GNU/Linux | powered x86_64 testing Debian image 😎  |
| none         | dummy template   | mostly used by debugging 😍 |


#### Templates and profiles

Unlike the original project here you can use the templates to create isos, or use those templates as profiles.

You can directly create default alpine iso by just use the "alpine" build in "profile template" named "alpine"

`makeiso -p=alpine`

You can [create derived profile](creating-profile.rst) from these templates running the following command: 

`makeiso -c=<Template name>`

After that, one directory with the name of the `<Template name>` will be created, based on the [profile "distro" definitions](porting-distribution.rst) and [profile format](creating-profile.rst).

You must tune the contents of the directory profile before produce the ISO image. For that get into the `<Template name>` directory just created and edit each file using the [Teaiso technology directives](Teaiso-technology.md).

#### Making the ISO image

To produce a ISO image file based on your directory profile, you must run the following command:

`makeiso -p=<absolute path of the created profile directory>`

Using your customized profile you can change the name of the profile directory to handle various flavours.

A brief example of making quick ISO its provided at the [starting-use-case.md](starting-use-case.md#customization-example)

#### Debug your process

You can just run the command and will provide standard out of the progress, also a log file is send to the `/var/log/makeiso.log` file. Extra debug information can be obtained by the `--debug` option.

The process uses [stages to determine the progress](Teaiso-technology.md#stage-runlevels) of the creation, 
so in each one some task are performed.

## See also

* [starting-use-case.md](starting-use-case.md)
* [Teaiso-technology.md](Teaiso-technology.md)
