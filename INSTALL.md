Teaiso INSTALL
==============

**The ISO generation tool for GNU/Linux**

For more information please consult the [doc/Installation.md](doc/Installation.md) document.

`git clone https://gitlab.com/venenux/venenux-teaiso && make build && make install`

**WARNING** for development we prefers **Codeberg**, but to ease the bandwich of codeberg we put the gitlab link.

## See also:

* [doc/Installation.md](doc/Installation.md)
* [doc/starting-use-case.md](doc/starting-use-case.md)
