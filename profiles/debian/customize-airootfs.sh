#!/usr/bin/env bash

# ====================================
#defaults base
apt install attr procps binutils findutils readline 
echo -e "root\nroot\n" | passwd root
# =====================================
# console base
apt install manpages man-db
apt install sed bash less groff bzip2 lsof nano coreutils dialog perl
# console admin tools
apt install pciutils util-linux
apt install aria2 curl wget
apt install htop mc rsync git git-svn subversion-tools nano
apt install unzip bzip2 p7zip-full xz-utils libarchive-tools lhasa
apt install fortunes fortune-mod cowsay
# ====================================
# device management, video and networking base
apt install dbus udev udevil acpi
# networking need in teaiso a network manager. currently only networkmanager works well, it depends on dbus
apt install networ-kmanager wpasupplican network-manager-vpnc network-manager-pptp network-manager-ssh network-manager-openvpn network-manager-openconnect network-manager-fortisslvpn
apt install libgudev libusb-compat usb-modeswitch usb-modeswitch-data
# hardware sound management
apt install alsa-utils alsa-oss alsa-tools
# hardware network management
apt install iw nmap fping ettercap wpasupplicant snmptrapd snmpd snmp
# hardware disk management
apt install partimage partimage ntfs-3g clonezilla
