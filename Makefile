build:
	make -C lib build
test:
	make -C lib test
install: build
	mkdir -p $(DESTDIR)/usr/lib/makeiso || true
	mkdir -p $(DESTDIR)/usr/bin || true
	make -C lib install DESTDIR=`realpath $(DESTDIR)`
	cp -prfv src/* $(DESTDIR)/usr/lib/makeiso/
	cp -prfv profiles $(DESTDIR)/usr/lib/makeiso/
	chmod +x -R $(DESTDIR)/usr/lib/makeiso/
	sed -i 's#teaiso=".*#teaiso="$(DESTDIR)/usr/lib/makeiso"#g'  makeiso
	install makeiso $(DESTDIR)/usr/bin/makeiso
	install logrotate $(DESTDIR)/etc/logrotate.d/makeiso

clean:
	make -C lib clean
uninstall:
	rm -rfv $(DESTDIR)/usr/bin/makeiso
	rm -rfv $(DESTDIR)/usr/lib/makeiso/
